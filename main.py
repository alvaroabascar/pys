import connexion

conn = connexion.App(__name__, port=3000, specification_dir="swagger/", debug=True)
conn.add_api("swagger.yaml")
app = conn.app
